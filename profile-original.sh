gfortran -p -o test_ave_value main.f90 acp.f90 fv.f90 matrix.f90 method.f90 sf.f90 
./test_ave_value
gprof test_ave_value > tav.output.txt
gprof test_ave_value | gprof2dot | dot -Tpng -o output.png
subl tav.output.txt
subl output.png