

    PROGRAM MAIN

    USE SDC
    USE ACP
    USE F_VALUE

    ! x-interval [0, 1].
    ! nx+1 grid points, labeled: kx = 0, 1, 2, ?, NX
    ! periodic boundary condition

    ! N = NX * NY * NV

    IMPLICIT NONE
    INCLUDE 'accu.h'
    REAL(WP), DIMENSION(N) :: UI,UO,UP

    REAL(WP), DIMENSION(1:NY,NV) :: UU
    REAL(WP), DIMENSION(1:NY, 3) :: VV

    REAL(WP), DIMENSION(3,3,   1:NY) :: TAU

    REAL(WP) :: TS,NTS1,NTS,TIME
    INTEGER :: K, MSTEP,INFO,KREJ,I,J,KK,K2,OS, SORDER, KIT, DEBUG
    INTEGER :: RESTART, EXIM, ORDER, ABORT, NST, PWW
    REAL(WP) :: EPS,MAXTS,MINTS,PP,PI, PWD,EP

    DATA MAXTS/1.D-1/, MINTS/1.D-7/

    INTEGER ::   KY

    REAL(WP) :: PN1, PN, PA, PDS, PDe, PCT, PV, PU0, FTIME, PAL
    REAL(WP) :: AL0, ETA, Pre2, Pre3, PG, CO2, CT2, PMGF




    CHARACTER(15) :: COUNT

    INTEGER :: METHODC, NODE, KS, NEX, IP, NCR


    INTEGER, DIMENSION(:), ALLOCATABLE :: seed


    OPEN (15,FILE="fort.15",ACTION="READ")
    READ (15,*) IP, KS
    READ (15,*) METHODC, NODE, NCR, NEX

    DEBUG = .FALSE.

    CALL ASSIGNSPACE(METHODC,NODE,N,KS,NEX,IP)

    !        CALL READ_3JS()

    OPEN(96,FILE="./fort.96")

    read(96,*) UBV

    ! uniform grid in y direction

    DO K = 0, NY
        YS(K) = DBLE(K)*HY
    END DO



    !        MSTEP = 0

    OPEN(95,FILE="./fort.95")

    READ(95,*) RESTART
    READ(95,*) PN,PAL,PN1,PU0,PA
    READ(95,*) PDe,PDS,PCT,PV,FTIME
    READ(95,*) NTS, MSTEP, EPS
    READ(95,*) ETA, Pre2, Pre3, PG, AL0
    READ(95,*) CO2, CT2, PMGF

    CLOSE(95)




    !    PRINT *, '************ Initial setting ***********************'
    !    PRINT *, '* N= ', PN, '* N1= ', PN1
    !    PRINT *, '* a= ', PA
    !    PRINT *, '* Initial time step size: ',NTS
    !    PRINT *, '* Final time: ', FTIME
    !    PRINT *, '* Error tolerance: ', EPS
    !    PRINT *, '* Order in  time: ',NCR+1
    !    IF(RESTART==1) THEN
    !       PRINT *, '* Restart from previous run'
    !    END IF
    !    PRINT *, '****************************************************'


    CALL SETANPE(PN, PAL, PN1, PDS, PCT, AL0, ETA, Pre2, Pre3, PG)   ! sf.f90

    CALL PVALUES(PN1, PAL, PN, PU0, PDS, PDE, PA, CO2, CT2, PMGF) ! acp.f90

    !CALL SET_NS_VALUE(ETA)                                  ! ns.f90

    ! initial value **********************************!

    IF(RESTART==0) THEN

        DO KY = 1, NY

            PP = 2.D0*YS(KY)
            U3(KY,:) = UBV*(1+ 0. *DCOS(2*3.1415926535D0*PP))

        END DO
        V3(:,1) = PV
        V3(:,2:3) = 0.

        DO KY = 1, NY
            V3(KY,1) = PV*YS(KY)
        END DO

        CALL RANDOM_SEED(size = J)
        ALLOCATE(seed(J))

        Do I = 1, KIT + 100
            CALL SYSTEM_CLOCK(COUNT=k)
        END DO

        seed = k + 37 * (/ (i - 1, i = 1, j) /)
        CALL RANDOM_SEED(PUT = seed)

        fu3 = 0
        CALL RANDOM_NUMBER(FU3)

        DEALLOCATE(seed)

        U3(:,2:9) = U3(:,2:9)+(FU3(:,2:9)-0.5D0)*2.D-2
        U3(:,10:16) = U3(:,10:16)+(FU3(:,10:16)-0.5D0)*0.5D-2
        U3(:,2:9) = U3(:,2:9)+(FU3(:,2:9)-0.5D0)*2.D-2

        !   V3(:,:,1:2) = (FU3(:,:,19:20)-0.5D0)*2.D-2

    ELSE IF(RESTART==1) THEN

        OPEN(81,FILE="./fort.81",ACTION="READ")
        READ(81,*) U3(1:NY,:)
        READ(81,*) V3(1:NY,:)
        CLOSE(81)

    ELSE

        OPEN(81,FILE="./fort.81",ACTION="READ")
        READ(81,*) U3
        CLOSE(81)

        V3 = 0.D0

    END IF

    UU = U3
    VV = V3
    !CALL FLOW_FIELD(V3)

    OPEN(97,FILE="./fort.97",ACTION="WRITE")
    OPEN(98,FILE="./fort.98",ACTION="WRITE")
    OPEN(99,FILE="./fort.99",ACTION="WRITE")


    ! *************************************************!




    TIME = 0._WP
    TS = NTS

    KREJ = 0

    KK = 0
    K2 = 0
    K = 1

    DO KIT = 1, 200000
        if(MOD(KIT-1, 200)==0) THEN
            write(*,*) KIT
        end if

        PP = MAXVAL(ABS(U3(:,2:9)))
        IF(PP<1.D-4) THEN
            CLOSE(97)
            CLOSE(98)
            CLOSE(99)
            OPEN(28, FILE="./fort.28", ACCESS="APPEND")
            WRITE(28,'(5(5x,F8.4))') PN, PAL, PN1, PU0, PCT
            WRITE(28,'(7(5x,F8.4))') PDe, PDS, eta, Pre2, Pre3, PG, AL0
            WRITE(28, *) KIT, TS, PP
            WRITE(28,* )
            CLOSE(28)
            stop
            !         If it converges, abort and restart
        END IF


        ! print result *******************************************************!

        !       if(TIME.GE.REAL(K2)*FTIME/100._WP) then

        IF(KIT>150000) THEN

            if(MOD(KIT-1, 200)==0) THEN
                !write(*,*) KIT

                K2 = K2 +1
                !WRITE(97, '(2x,G13.6)') Time

                ! KY = NY/32
                KY=1
                !U3(0,:) = UBV
                WRITE(97, '(9(2x,G13.6))') &
                    Time, UBV(1:4),UBV(14),0, 0, 0
                DO J = 1, NY, KY
                    DO OS = 1, 14
                        IF(ABS(U3(J,OS))<1D-99) U3(J,OS) = 0
                    END DO
                    WRITE(97, '(9(2x,G13.6))') &
                        Time,U3(J,1:4),U3(J,14),V3(J,:)

                END DO

                WRITE(98, '(2x,G13.6)') Time
                DO J = 1, NY, KY
                    WRITE(98, '(9(2x,G13.6))') U3(J,5:13)
                END DO


            end if

            !if(MOD(KIT-1, 200)==0) THEN
            !    WRITE(99, '(2x,G13.6)') Time
            !    DO J = 1, NY, KY
            !        WRITE(99, '(16(2x,G13.6))') U3(J,10:25)
            !    END DO
            !end if

        END IF

        IF(TIME>FTIME) THEN
            PRINT *, 'rejected steps: ', KREJ
            PRINT *, 'Successful steps: ', K
            OPEN(81,FILE="./fort.81",ACTION="WRITE")
            write(81,*) U3(1:NY,:)
            write(81,*) V3(1:NY,:)
            STOP
        END IF

        !*******************************************************************!

        TS = NTS


        ! Simple shear

        !DO KY = 1, NY
        !    V3(KY,1) = PV*YS(KY)
        !END DO
        V3(:,2:3) = 0.D0

        DVDS(:,:,:) = 0.D0
        DVDS(:,1,2) = PV
        go to 1006


        ! Compute Pressure

        !*** compute FV3A = Div(Tau_e):

        CALL DTAUM(U3,TAU,TIME)

        !*** solve for pressure:

        !      If need to decrease the time step size
300     CONTINUE

        V3 = VV

        TS = NTS

        ! Compute Velocity
        F_PARA = 1

        !  UI(1:N3) = RESHAPE(V3( 1:NY,1:3),(/N3/))

        OS = 0
        DO KY = 1, NY
            UI(OS+1:OS+3) = V3( KY,:)
            OS = OS + 3
        END DO

        CALL METHOD(UI(1:N3),UO(1:N3),TIME,TS,NCR,UP(1:N3))

        IF(MSTEP==1) THEN
            INFO = 0
            CALL STEPSIZE(UO(2:N3:3), UP(2:N3:3), EPS, TS, NTS, DBLE(NCR+1), INFO)
            IF(NTS.GT.MAXTS) NTS = MAXTS
            IF(NTS.LT.MINTS) THEN
                NTS = MINTS
                INFO = 0
                !              PRINT *, 'Too small time step size generated!',ts,nts
            END IF

            IF(INFO==1) THEN
                KREJ = KREJ + 1
                WRITE(89,*) ' velocity rejected', KREJ, K
                GO TO 300
            END IF
        END IF

        !  V3( 1:NY,:) = RESHAPE(UO(1:N3),(/NX,NY,3/))

        OS = 0
        DO KY = 1, NY

            V3( KY,:) = UO(OS+1:OS+3)
            OS = OS + 3

        END DO

        !      If not time integrate velocity

        !CALL FLOW_FIELD(V3)

1006    CONTINUE

        ! Compute Probability Distribution Function

        F_PARA = 2

        U3 = UU

        !       UI = RESHAPE(U3( 1:NY,:),(/N/))

        OS = 0
        DO KY = 1, NY

            UI(OS+1:OS+NV) = U3( KY,:)
            OS = OS + NV

        END DO

        CALL METHOD(UI,UO,TIME,TS,NCR,UP)

        IF(MSTEP==1) THEN
            !     NTS1 = 1d-1
            NTS1 = NTS
            INFO = 0
            CALL STEPSIZE(UO(1:N:NV), UP(1:N:NV), EPS, TS, NTS, DBLE(NCR+1), INFO)
            IF(NTS.GT.MAXTS) NTS = MAXTS
            IF(NTS.LT.MINTS) THEN
                NTS = MINTS
                INFO = 0
                !            PRINT *, 'Too small time step size generated!',ts,nts
            END IF

            IF(INFO==1) THEN
                KREJ = KREJ + 1
                write(89,*) '  PDF rejected', KREJ, K
                GO TO 300
            END IF

            IF(NTS>NTS1) NTS = NTS1

        END IF

        !       U3( 1:NY,:) = RESHAPE(UO,(/NX,NY,NV/))

        OS = 0
        DO KY = 1, NY
            U3( KY,:) = UO(OS+1:OS+NV)
            OS = OS + NV
        END DO

        U3(NY,:)=UBV

        !      save a old copy in case reducing the time step size and redo

        VV = V3
        UU = U3

        TIME = TIME + TS

    END DO

    END PROGRAM MAIN
