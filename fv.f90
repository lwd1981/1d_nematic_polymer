    MODULE F_VALUE

    USE LCP_2D

    PRIVATE WP
    INCLUDE 'accu.h'

    CONTAINS


    !**********************************************************************!

    SUBROUTINE FVALUE(U, T, F)
    IMPLICIT NONE

    REAL(WP), DIMENSION(:), INTENT(IN)  :: U
    REAL(WP), DIMENSION(:), INTENT(OUT) :: F
    REAL(WP) :: T


    IF(F_PARA==1) THEN
        !CALL MOMENTUM(U,T,F)
    ELSE IF(F_PARA==2) THEN
        CALL SMOLUCHOWSKI(U,T,F)
    ELSE
        PRINT *, "Wrong F_PARA! STOP"
        STOP
    END IF

    END SUBROUTINE FVALUE


    !**********************************************************************!


    SUBROUTINE FVALUE_EX(U, T, F)
    IMPLICIT NONE
    REAL(WP), DIMENSION(:) :: U, F
    REAL(WP) :: T


    IF(F_PARA==1) THEN
        !        CALL MOMENTUM(U,T,F)
    ELSE
        CALL SMOLUCHOWSKI(U,T,F)
    END IF

    END SUBROUTINE FVALUE_EX


    SUBROUTINE FVALUE_EXC(U, T, F)
    IMPLICIT NONE
    REAL(WP), DIMENSION(:) :: U, F
    REAL(WP) :: T

    CALL FVALUE(U,T,F)

    END SUBROUTINE FVALUE_EXC


    SUBROUTINE FVALUE_IM(U, T, F)
    IMPLICIT NONE
    REAL(WP), DIMENSION(:) :: U, F
    REAL(WP) :: T

    F = 0

    END SUBROUTINE FVALUE_IM


    !*****************************************************************!



    SUBROUTINE PRECONDITION(F, V, DT)
    IMPLICIT NONE

    REAL(WP), DIMENSION(:), INTENT(IN) :: F
    REAL(WP), DIMENSION(:), INTENT(OUT) :: V
    REAL(WP) :: DT

    V = F

    END SUBROUTINE PRECONDITION



    SUBROUTINE ITERSOLV(F, V, DT)
    IMPLICIT NONE

    REAL(WP), DIMENSION(:), INTENT(IN) :: F
    REAL(WP), DIMENSION(:), INTENT(OUT) :: V
    REAL(WP) :: DT
    INTEGER :: OS, K, KY, KZ

    IF(F_PARA==1) THEN
        V = F
        RETURN
    ELSE IF(F_PARA==2) THEN
        V = F
        RETURN
    END IF

    END SUBROUTINE ITERSOLV


    SUBROUTINE BDF_SOLV(F, V, CA, TF)
    IMPLICIT NONE

    REAL(WP), DIMENSION(:), INTENT(IN) :: F
    REAL(WP), DIMENSION(:), INTENT(OUT) :: V
    REAL(WP) :: CA, TF

    V = F

    END SUBROUTINE BDF_SOLV


    !*****************************************************************!


    FUNCTION EXACT_VALUE(N,T)
    IMPLICIT NONE
    INTEGER :: N
    REAL(WP), DIMENSION(N) :: EXACT_VALUE
    REAL(WP) :: T

    EXACT_VALUE = 0

    END FUNCTION EXACT_VALUE


    SUBROUTINE ERROR(U,T,ABSE)
    IMPLICIT NONE
    REAL(WP), DIMENSION(:) :: U
    REAL(WP), DIMENSION(SIZE(U)) :: ERR
    REAL(WP) :: T, ABSE
    INTEGER :: K, N

    ERR = EXACT_VALUE(SIZE(U),T)-U

    ABSE = SQRT(ERR(1)*ERR(1)+ERR(2)*ERR(2))

    !        PRINT *, T, ERR(2), ABS(ERR(2)-U(2))

    END SUBROUTINE ERROR


    END MODULE F_VALUE


