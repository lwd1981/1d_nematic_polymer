# %matplotlib inlineaaaaaa
import matplotlib.pyplot as plt
import numpy as np
from numpy import ma
# import math

# read file first
with open("fort.97", "r") as ins:
    array = []
    for line in ins:
        temp= [float(x) for x in line.split()]
        array.append(temp)
        
print array[0][1]

print 'number of lines: '+str(len(array))

# the unmodified grid
numGrid=0
while array[numGrid][0] == array[numGrid+1][0]:
    numGrid=numGrid+1
numGrid+=1    
print 'number of Grids: '+str(numGrid)

numTimeStep=len(array)/numGrid
print 'number of tStep: '+str(numTimeStep)

# X is actually y, Y is t
X,Y = np.meshgrid(np.arange(0, len(array)/numGrid,1 ), np.arange(0,numGrid,1))
# X,Y = np.meshgrid(np.arange(0, 0.01*len(array)/64,0.01*1 ), np.arange(0,0.01*64,0.01*1))
# X,Y = np.meshgrid(np.arange(0, 0.01*len(array)/64/32,0.01*len(array)/64/32 ), np.arange(0,0.01*64/32,0.01*1/32))
# X, Y = np.meshgrid(np.arange(0, 2 * np.pi, .2), np.arange(0, 2 * np.pi, .2))
# X, Y = np.meshgrid(np.arange(0, 1, 1),np.arange(0, 2 * np.pi, .2) )

# t = np.linspace(1,len(array)/64,len(array)/64)
# y = np.linspace(0,64-1,64)
# (X,Y) = np.meshgrid(t,y)

# U = X
# V = Y
U= [-1*row[4] for row in array]
V= [row[2] for row in array]

#**********fix the plot****************

tempU = [0] * len(U)
tempV = [0] * len(V)

it=0
for t in range(0,numTimeStep):
	counter=t
	for y in range(0,numGrid):
		# print it,counter
		tempU[counter]=U[it]
		tempV[counter]=V[it]
		it=it+1
		counter=counter+numTimeStep
U=tempU
V=tempV
#**********fix the plot****************

# normalize
# UV=zip(U, V)
# # print UV
# UV= [x/np.linalg.norm(x) for x in UV]
# # print UV
# U=[x[0]for x in UV]
# V=[x[1]for x in UV]
# print U[5]==UV[5][0]

# # testing
# U = np.cos(X)
# V = np.sin(Y)

# 1
plt.figure()
Q = plt.quiver(X, Y,U, V,
               pivot='mid', color='r', units='width')
# Q = plt.quiver(X, Y,U, V)
# qk = plt.quiverkey(Q, 0.5, 0.92, 2, r'$2 \frac{m}{s}$', labelpos='W',
#                    fontproperties={'weight': 'bold'})
l, r, b, t = plt.axis()
dx, dy = r - l, t - b

plt.axis([l - 0.05*dx, r + 0.05*dx, b - 0.05*dy, t + 0.05*dy])
plt.xlabel('Time')
plt.ylabel('Y')
plt.title('t,y plot')
# save to eps and png
plt.savefig('./t'+str(len(array)/numGrid)+'.eps', format='eps', dpi=1000)
plt.savefig('./t'+str(len(array)/numGrid)+'.png')

# plt.title('Minimal arguments, no kwargs')

# # 2
# plt.figure()
# Q = plt.quiver(X, Y, U, V, units='width')
# # qk = plt.quiverkey(Q, 0.9, 0.95, 2, r'$2 \frac{m}{s}$',
# #                    labelpos='E',
# #                    coordinates='figure',
# #                    fontproperties={'weight': 'bold'})
# plt.axis([-1, 7, 60, 65])
# plt.title('scales with plot width, not view')
# # plt.show()
# # 3
# plt.figure()
# # print X
# # print Y
# # print X[::2, ]
# # print Y[::2, ::1]
# # print V
# # print U[::2]
# # print V[::1, ::1]
# Q = plt.quiver(X[::7, ::1], Y[::7, ::1], U[::7], V[::7],
#                pivot='mid', color='r', units='inches')
# # qk = plt.quiverkey(Q, 0.5, 0.03, 1, r'$1 \frac{m}{s}$',
# #                    fontproperties={'weight': 'bold'})
# # plt.plot(X[::1, ::1], Y[::1, ::1], 'k.')
# # plt.axis([-1, 7, -1, 7])
# # plt.title("pivot='mid'; every third arrow; units='inches'")
# # plt.show()

# # 4
# plt.figure()
# M = np.hypot(U, V)
# Q = plt.quiver(X, Y, U, V, M,
#                units='x',
#                pivot='tip',
#                width=0.022,
#                scale=1 / 0.15)
# qk = plt.quiverkey(Q, 0.9, 1.05, 1, r'$1 \frac{m}{s}$',
#                    labelpos='E',
#                    fontproperties={'weight': 'bold'})
# plt.plot(X, Y, 'k.')
# plt.axis([-1, 7, -1, 7])
# # plt.axis([l - 0.05*dx, r + 0.05*dx, b - 0.05*dy, t + 0.05*dy])

# plt.title("scales with x view; pivot='tip'")

# # 5
# plt.figure()
# Q = plt.quiver(X[::1, ::7], Y[::1, ::7], U[ ::7], V[ ::7],
#                color='r', units='x',
#                linewidths=(2,), edgecolors=('k'), headaxislength=5)
# qk = plt.quiverkey(Q, 0.5, 0.03, 1, r'$1 \frac{m}{s}$',
#                    fontproperties={'weight': 'bold'})
# # plt.axis([-1, 7, -1, 7])
# plt.axis([l - 0.05*dx, r + 0.05*dx, b - 0.05*dy, t + 0.05*dy])

# plt.title("triangular head; scale with x view; black edges")

# # 6
# # plt.figure()
# # U=np.array(U)
# # M = np.zeros(U.shape, dtype='bool')
# # M[U.shape[0]/3:2*U.shape[0]/3,
# #   U.shape[1]/3:2*U.shape[1]/3] = True
# # U = ma.masked_array(U, mask=M)
# # V = ma.masked_array(V, mask=M)
# # Q = plt.quiver(U, V)
# # qk = plt.quiverkey(Q, 0.5, 0.92, 2, r'$2 \frac{m}{s}$', labelpos='W',
# #                    fontproperties={'weight': 'bold'})
# # l, r, b, t = plt.axis()
# # dx, dy = r - l, t - b
# # plt.axis([l - 0.05 * dx, r + 0.05 * dx, b - 0.05 * dy, t + 0.05 * dy])
# # plt.title('Minimal arguments, no kwargs - masked values')


# plt.show()
            


