    MODULE SDC

    USE F_VALUE
    USE SPEC_DEFE_CORR

    INTEGER, PRIVATE, SAVE :: KS, NEX, IP

    PRIVATE :: WP, SUBTS, TIM, UA, UB, FS
    INCLUDE 'accu.h'
    REAL(WP), DIMENSION(:,:), ALLOCATABLE :: UA,UB,FS
    REAL(WP), DIMENSION(:), ALLOCATABLE :: SUBTS, TIM
    SAVE SUBTS, TIM, UB

    !   N: number of all nodes
    !   M: number of all nodes except the beginning point, if it is a node
    !  NP: number of all nodes plus the begining point, if it is not a node
    !  KS: number of steps (KS=1: single step; KS>1: multi-step method)
    ! NEX: extrapolation or spectral integration after corrections

    CONTAINS

    !**********************************************************!

    SUBROUTINE ASSIGNSPACE(MI, NI, ND, KI, NX, IPP)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: MI, NI, ND, KI, NX, IPP
    INTEGER :: ERR

    !  MI : What kind of nodes to use
    !       MI = 1 : Gauss
    !       MI = 2 : RADAU
    !       MI = 3 :
    !  NI : How many nodes to use
    !  ND : Dimension of the ODE system
    !  KI : =1 (single step method); >1 (multi-step method)
    !  NX : =1 (use extrapolation); =0 (use spectral integration)
    ! IPP : =1 (use preconditioning); =0 (no preconditioning)


    KS = KI
    NEX = NX
    IP = IPP

    CALL INTEMATX(MI, NI)

    ALLOCATE(UA(ND,NP),UB(ND,NP),FS(ND,NP),SUBTS(M),TIM(NP),STAT=ERR)
    IF (ERR/=0) THEN
        PRINT *, "METHOD-METHOD-UA : Allocation failed"
        STOP
    END IF

    UA = 0._WP
    UB = 0._WP
    FS = 0._WP
    SUBTS = 0._WP

    END SUBROUTINE ASSIGNSPACE

    !**********************************************************!

    SUBROUTINE FREESPACE()
    IMPLICIT NONE

    DEALLOCATE(UA, UB, SUBTS)

    END SUBROUTINE FREESPACE

    !**********************************************************!

    SUBROUTINE INITIAL_VALUE(TS,ND)
    IMPLICIT NONE
    REAL(WP), INTENT(IN) :: TS
    INTEGER :: ND, K

    CALL SUBINTERVAL(TS,SUBTS)

    IF(MI==1) THEN
        TIM(1) = -TS+SUBTS(1)
    ELSE IF(MI==2 .OR. MI==3) THEN
        TIM(1) = -TS
    END IF
    DO K = 2, M     ! old NP-1
        TIM(K) = TIM(K-1) + SUBTS(K)
    END DO
    TIM(NP) = 0._WP

    DO K = 1, NP
        UB(:,K) = EXACT_VALUE(ND,TIM(K))
    END DO


    END SUBROUTINE INITIAL_VALUE



    SUBROUTINE INITIAL_VALUE_0(TS,ND)
    IMPLICIT NONE
    REAL(WP), INTENT(IN) :: TS
    INTEGER :: ND, K

    TIM(NP) = 0._WP
    DO K = NP-1, 1, -1
        TIM(K) = TIM(K+1) - TS
    END DO

    DO K = 1, NP
        UB(:,K) = EXACT_VALUE(ND,TIM(K))
    END DO

    END SUBROUTINE INITIAL_VALUE_0


    !**********************************************************!

    SUBROUTINE METHOD(UI,UO,T,TS,NCR,UP)
    IMPLICIT NONE
    REAL(WP), DIMENSION(:), INTENT(IN) :: UI
    REAL(WP), DIMENSION(:), INTENT(OUT) :: UO
    REAL(WP), DIMENSION(:), OPTIONAL :: UP
    REAL(WP), INTENT(IN) :: T,TS
    INTEGER :: NCR

    REAL(WP) :: STS
    INTEGER :: K

    IF(NCR==0) THEN
        CALL START_METHOD(UI,UO,T,TS,1)
        !	print *, "METHOD ", MAXVAL(ABS(UO))

        RETURN
    END IF

    !*** Initial value

    UA(:,1) = UI
    UA(:,2:NP) = 0._WP

    !*** Determine sub-time stepsize

    CALL SUBINTERVAL(TS,SUBTS)

    !*** Compute first order solutions

    CALL ROUGH_SOLU(UA,T,TS)

    IF(NCR==2) THEN
        !      CALL EXTRAP(UA,UP)
    END IF

    !*** Generate more accurate solutions

    IF(.NOT.PRESENT(UP)) THEN
        CALL CORRECTION(UA,UB,T,TS,NCR)
    ELSE
        CALL CORRECTION(UA,UB,T,TS,NCR,UP)
    END IF

    ! not use extrapolation, did not compute coefficients this time. add later
    !   CALL EXTRAP(UB,UO)

    ! Return if extrapolation is used;
    ! otherwise, do the following for integration.

    STS = 0
    CALL FVALUE(UB(:,1), T, FS(:,1))
    DO K = 2, NP
        STS = STS + SUBTS(K-1)
        CALL FVALUE(UB(:,K), T+STS, FS(:,K))
    END DO

    IF(M==N) THEN
        CALL SIGL(UI,FS(:,2:NP),UO,TS)
    ELSE
        CALL SIGL(UI,FS,UO,TS)
    ENDIF


    ! Following are used in SBDF method

    DO K = 2, NP
        TIM(K-1) = TIM(K)
        UB(:,K-1) = UB(:,K)
    END DO
    TIM(NP) = T+TS
    UB(:,NP) = UO

    END SUBROUTINE METHOD


    !**********************************************************!


    !**********************************************************!

    SUBROUTINE ROUGH_SOLU(U,T,TS)
    IMPLICIT NONE
    REAL(WP), DIMENSION(:,:), INTENT(INOUT) :: U
    REAL(WP) :: T,TS
    INTEGER :: K, J

    IF(KS==1) THEN  ! single step method

        TIM(1) = T
        DO K = 1, M
            CALL START_METHOD(U(:,K),U(:,K+1), TIM(K), SUBTS(K), K)
            TIM(K+1) = TIM(K) + SUBTS(K)
        END DO

    ELSE

        DO K = 1, M
            CALL SBDF(UB, U(:,K+1),TIM(NP)+SUBTS(K),KS)
            DO J = 2, NP
                TIM(J-1) = TIM(J)
                UB(:,J-1) = UB(:,J)
            END DO
            TIM(NP) = TIM(NP)+SUBTS(K)
            UB(:,NP) = U(:,K+1)

        END DO

        TIM(1) = T
        DO K = 1, M
            TIM(K+1) = TIM(K) + SUBTS(K)
        END DO

    END IF


    END SUBROUTINE ROUGH_SOLU

    !**********************************************************!


    !**********************************************************!

    SUBROUTINE CORRECTION(U, UT, T, TS, KMAX, UP)
    IMPLICIT NONE
    REAL(WP), DIMENSION(:,:), INTENT(IN) :: U
    REAL(WP), DIMENSION(:,:), INTENT(OUT) :: UT
    REAL(WP), DIMENSION(:), OPTIONAL :: UP
    REAL(WP), DIMENSION(:,:), ALLOCATABLE :: UB
    REAL(WP) :: T, TS
    INTEGER :: KMAX, K, ERR

    !  U: first order solution
    ! UT: last corrected solution
    ! UB: solution of the neighbor equation

    ALLOCATE(UB(SIZE(U,DIM=1),SIZE(U,DIM=2)), STAT=ERR)
    IF (ERR/=0) THEN
        PRINT *, "METHOD-CORRECTION-UB : Allocation failed"
        STOP
    END IF

    UT = U
    UB(:,1) = U(:,1)

    DO K = 1, KMAX

        !      IF(K.EQ.(KMAX-1) .AND. PRESENT(UP)) THEN
        !         CALL EXTRAP(UT,UP)
        !      END IF

        CALL NEIGHBOR_SOLU(U,UT,UB,T,TS)
        UT(:,2:) = UT(:,2:) + (U(:,2:) - UB(:,2:))

    END DO

    DEALLOCATE(UB)

    END SUBROUTINE  CORRECTION


    !**********************************************************!


    !**********************************************************!

    SUBROUTINE NEIGHBOR_SOLU(UA,UT,UB,T,TS)
    IMPLICIT NONE
    REAL(WP), DIMENSION(:,:), INTENT(IN) :: UA
    REAL(WP), DIMENSION(:,:), INTENT(IN) :: UT
    REAL(WP), DIMENSION(:,:), INTENT(INOUT) :: UB
    REAL(WP), DIMENSION(:,:), ALLOCATABLE :: SPI
    REAL(WP) :: T,TS,CT
    INTEGER :: K, ERR

    !  U: first order solution
    ! UT: last corrected solution
    ! UB: solution of the neighbor equation

    ALLOCATE(SPI(SIZE(UT,DIM=1),NP), STAT=ERR)
    IF (ERR/=0) THEN
        PRINT *, "METHOD-NEIGHBOR_SOLU-SPI : Allocation failed"
        STOP
    END IF

    CT = T
    CALL FVALUE(UT(:,1), CT, FS(:,1))
    DO K = 2, NP
        CT = CT + SUBTS(K-1)
        CALL FVALUE(UT(:,K), CT, FS(:,K))
    END DO

    IF(M==N) THEN
        CALL SPECINT(FS(:,2:NP),SPI,TS)
    ELSE
        CALL SPECINT(FS,SPI,TS)
    ENDIF

    DO K = 1, M
        SPI(:,K) = SPI(:,K)-(UT(:,K+1)-UT(:,K))
    END DO

    CT = T
    DO K = 1, M
        CALL MD_ONE_STEP(UB(:,K),UB(:,K+1), CT, SUBTS(K), SPI(:,K))
        CT = CT + SUBTS(K)
    END DO

    DEALLOCATE(SPI)

    END SUBROUTINE NEIGHBOR_SOLU


    !**********************************************************!


    !**********************************************************!

    SUBROUTINE MD_ONE_STEP(UOLD, UNEW, T, TS, CORR)
    IMPLICIT NONE
    INCLUDE "accu.h"
    REAL(WP), DIMENSION(:), INTENT(IN) :: UOLD, CORR
    REAL(WP), DIMENSION(:), INTENT(OUT) :: UNEW
    REAL(WP), DIMENSION(:), ALLOCATABLE :: F
    REAL(WP) :: T, TS
    INTEGER :: ERR

    ALLOCATE(F(SIZE(UOLD)), STAT=ERR)
    IF (ERR/=0) THEN
        PRINT *, "METHOD-ONE_STEP-F : Allocation failed"
        STOP
    END IF

    CALL FVALUE_EX(UOLD, T, F)

    F = UOLD + TS * F - CORR

    CALL ITERSOLV(F, UNEW, TS)

    DEALLOCATE(F)

    END SUBROUTINE MD_ONE_STEP

    !**********************************************************!

    !**********************************************************!

    SUBROUTINE START_METHOD(UOLD, UNEW, T, TS, K)
    IMPLICIT NONE
    INCLUDE "accu.h"
    REAL(WP), DIMENSION(:), INTENT(IN) :: UOLD
    REAL(WP), DIMENSION(:), INTENT(OUT) :: UNEW
    REAL(WP), DIMENSION(:), ALLOCATABLE :: F
    REAL(WP) :: T, TS
    INTEGER :: K, ERR

    ALLOCATE(F(SIZE(UOLD)), STAT=ERR)
    IF (ERR/=0) THEN
        PRINT *, "METHOD-ONE_STEP-F : Allocation failed"
        STOP
    END IF

    IF(IP==1) THEN

        CALL FVALUE(UOLD, T, FS(:,K))

        F = UOLD + TS * FS(:,K)

        CALL PRECONDITION(F, UNEW, TS)

    ELSE

        CALL FVALUE_EX(UOLD, T, F)

        F = UOLD + TS * F

        CALL ITERSOLV(F, UNEW, TS)

        !	print *, MAXVAL(ABS(F)), MAXVAL(ABS(UOLD)), MAXVAL(ABS(UNEW))
    END IF

    DEALLOCATE(F)

    END SUBROUTINE START_METHOD

    !**********************************************************!


    !**********************************************************!

    ! Semi-implicit BDF scheme
    ! UB   : solutions at previous times
    ! UNEW : solution at the new time
    ! TF   : the bew time
    ! KS   : number of steps, also the order of the scheme

    SUBROUTINE SBDF(UB, UNEW, TF, KS)
    IMPLICIT NONE
    INCLUDE "accu.h"
    REAL(WP), DIMENSION(:,:), INTENT(IN) :: UB
    REAL(WP), DIMENSION(:), INTENT(OUT) :: UNEW
    REAL(WP), DIMENSION(:), ALLOCATABLE :: F
    REAL(WP) :: TF, CA, CB
    INTEGER :: KS, K, I, J, NPP, ERR

    ALLOCATE(F(SIZE(UNEW)), STAT=ERR)
    IF (ERR/=0) THEN
        PRINT *, "METHOD-ONE_STEP-F : Allocation failed"
        STOP
    END IF

    NPP = SIZE(UB,DIM=2);

    DO K = NPP-KS+1, NPP
        CALL FVALUE_EX(UB(:,K), TIM(K), FS(:,K))
    END DO

    F = 0._WP
    CA = 0._WP
    DO J = NPP-KS+1, NPP
        CA = CA + 1._WP/(TF-TIM(J))
        CB = 1._WP
        DO I = NPP-KS+1, NPP
            IF(I/=J) THEN
                CB = CB*(TF-TIM(I))/(TIM(J)-TIM(I))
            END IF
        END DO
        F = F + CB*(FS(:,J)+UB(:,J)/(TF-TIM(J)))
    END DO

    CALL BDF_SOLV(F, UNEW, CA, TF)

    DEALLOCATE(F)

    END SUBROUTINE SBDF

    !**********************************************************!

    !**********************************************************!


    !**********************************************************!

    SUBROUTINE STEPSIZE(U1,U2,EPS,TSO,TS,ORDER,INFO)
    IMPLICIT NONE
    INCLUDE "accu.h"
    REAL(WP), DIMENSION(:) :: U1, U2
    REAL(WP) :: EPS, TSOO, TSO, TS,TSNEW1,TSNEW2
    REAL(WP) :: QERR, OQERR, ORDER, QI, QD, FAC, PN,Q0
    INTEGER :: INFO, ICHOOSE
    DATA TSOO/1.D0/
    DATA OQERR/1.D0/
    SAVE OQERR, TSOO

    REAL(WP) :: LASTREJ
    DATA LASTREJ/1.D0/
    SAVE LASTREJ

    INTEGER :: CC
    DATA CC/0/
    SAVE CC

    INFO = 0

    IF(TSOO>0.9D0) TSOO = TSO

    FAC=0.85*(TSO/TSOO)

    ichoose = 1
    if(ichoose.eq.1) then

        Q0 = DOT_PRODUCT(U2,U2)
        U2 = ABS(U1-U2)/EPS
        QI = DOT_PRODUCT(U1,U1)
        QD = DOT_PRODUCT(U2,U2)
        QERR=SQRT(QD/(QI+1.D-20))

    else

        QERR=MAXVAL(ABS(U1-U2)/ABS(U1+1.D-60))

    end if

    IF(QERR.GT.1.D0) THEN
        !           FAC = 0.9*TSO/TSOO
        LASTREJ = TSO
        CC = 0
        INFO = 1
    ELSE
        CC = CC + 1
        IF(CC>30) THEN
            CC = 15
            LASTREJ = 1.01D0*LASTREJ
        END IF
    END IF

    !        print *, CC, LASTREJ

    IF(QERR.LT.1.D-100) THEN
        TS = 2.*TSO
        RETURN
    END IF

    PN=1.D0/(ORDER+1)
    !        TSNEW1=FAC*TSO/(QERR**PN)*(OQERR/QERR)**PN
    TSNEW1=FAC*TSO/(QERR**PN)*(OQERR/QERR)**PN
    TSNEW2=0.85*TSO/(QERR**PN)

    !	TS = MIN(TSNEW1, TSNEW2)

    TS = TSNEW2
    !	IF(QERR>1.5*OQERR) TS = TSNEW1   ! not good
    !	IF(QERR>OQERR .AND. TSO<TSOO .AND. TS>TSO) TS = TSO

    IF(TS>1.5*TSO) TS = 1.5D0*TSO
    IF(TS .GT. 1.0*LASTREJ) TS = 0.99*LASTREJ

    TSOO = TSO
    OQERR = QERR

    !	write(6, '(4(2x,G12.5))') QERR,TSNEW1, TSNEW2, TS

    !	print *, tsnew1, tsnew2

    END SUBROUTINE STEPSIZE

    !**********************************************************!


    END MODULE SDC
