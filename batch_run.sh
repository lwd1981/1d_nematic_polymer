gfortran main.f90 acp.f90 fv.f90 matrix.f90 method.f90 sf.f90 > compiler_message
# time ./a.out

# for (( i=1; i <= 5; i++ ))
# do
#  echo "Random number $i: $RANDOM"
# done
touch t20.png
touch t20.eps

size=3
# for i in $(seq 1 $size); 
for (( i=3; i <= $size; i++ ))
	do echo $i
		cp ./fort.95.pv1.pn$i ./fort.95
		# COMMAND="echo $i"
		COMMAND="time ./a.out"
		eval $COMMAND;
		# time $ eval $COMMAND;

		# take the last 20 time steps to plot
		tail -1300 fort.97 > output
		mv output fort.97
		python plot.py

		# save away the result
		mv t20.png t20_pv1_pn$i.png
		mv t20.eps t20_pv1_pn$i.eps
		mv fort.97 fort.97.pv1_pn$i		
done
# tail -1300 fort.97 > output
# mv output fort.97
# python plot.py