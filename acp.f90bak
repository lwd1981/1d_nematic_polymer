    MODULE ACP


    INTEGER, PARAMETER :: LMAX = 10, LP = (LMAX+1)*(LMAX+1)

    PRIVATE WP

    INTEGER, PARAMETER :: WP = Kind(1.d0)

    ! ***      (L, M) ---> L*(L+1)+M         *** !
    ! ***  L from 0 to LMAX; M from -L to L   *** !

    REAL(WP), DIMENSION(0:LP-1,0:LP-1,0:LMAX), SAVE :: SIGMA, BETA

    REAL(WP), DIMENSION(0:LMAX,-LMAX:LMAX,-1:1), SAVE :: GAMA

    REAL(WP), SAVE, PRIVATE :: PN1,PN,PU0,PDS,PDE,PA,ALPHA, CAO2, CAT2, PMGF

    CONTAINS

    SUBROUTINE READ_3JS()
    IMPLICIT NONE

    INTEGER :: L,M,J,P,K
    REAL(WP) :: R1, R2, R3

    SIGMA = 0
    BETA = 0
    GAMA = 0

    OPEN(16,FILE="../data/SigmaBeta.dat",ACTION="READ")

    DO WHILE(.TRUE.)
        READ(16,*,END=200) J,P,L,M,K,R1,R2
        IF(J<=LMAX .AND. ABS(P)<=J .AND. L<=LMAX .AND. ABS(M)<=L .AND. K<=LMAX) THEN
            SIGMA(J*(J+1)+P,L*(L+1)+M,K) = R1
            BETA (J*(J+1)+P,L*(L+1)+M,K) = 0.5*R2
        END IF
    END DO
200 CONTINUE
    CLOSE(16)

    OPEN(17,FILE="../data/almp.dat",ACTION="READ")

    DO WHILE(.TRUE.)
        READ(17,*,END=300) L,M, R1,R2,R3
        IF(L<=LMAX .AND. ABS(M)<=L) THEN
            GAMA(L,M,-1) = R1
            GAMA(L,M, 0) = R2
            GAMA(L,M, 1) = R3
        END IF
    END DO
300 CONTINUE
    CLOSE(17)

    print *, "Done reading data.", maxval(sigma),maxval(gama)

    END SUBROUTINE READ_3JS



    SUBROUTINE PVALUES(PPN1, PPAL, PPN2, PPU0, PPDS, PPDE, PPA, CO2, CT2, PF)
    IMPLICIT NONE

    REAL(WP) :: PPN1, PPAL, PPN2, PPU0, PPDS, PPDE, PPA, CO2, CT2, PF

    PN1 = PPN1
    PN  = PPN2
    PDS = PPDS
    PU0 = PPU0
    PDE = 1.D0/PPDE
    PA  = PPA
    ALPHA = PPAL
    CAO2 = CO2
    CAT2 = CT2
    PMGF = PF

    END SUBROUTINE PVALUES



    SUBROUTINE DFDT(AI, D1AIX, D2AIX, D1AIY, D2AIY, DV, FV)
    IMPLICIT NONE

    INTEGER :: L,M,J,P,K
    REAL(WP) :: PE1, PE2, PE3, ZZZ

    REAL(WP), DIMENSION(3,3) :: DV
    REAL(WP), DIMENSION(:) :: AI, D1AIX, D2AIX, D1AIY, D2AIY, FV
    REAL(WP), DIMENSION(0:LMAX, -LMAX:LMAX) :: A, D1AX, D2AX, D1AY, D2AY, B, C, D, E

    REAL(WP) :: Pi, Pi2, C1, C2, A0, C3, C4, D1, D2, D3, RC, DRR
    DATA Pi/3.1415926535897932_WP/
    !     DATA A0/3.5449077018110318_WP/   ! Sqrt[4Pi]
    DATA A0/12.566370614359172_WP/   ! 4Pi
    DATA Pi2/9.86960440108935862_WP/  ! \Pi*\Pi
    DATA C1/-2.51327412287183459_WP/  ! 8Pi/15*(-3/2)
    DATA C2/1.05688727936160294_WP/   ! 2/3 Sqrt{4Pi/5}
    DATA C3/1.1816359006036772_WP/    ! 2/3 Sqrt{Pi}
    DATA C4/0.6472086375185664_WP/    ! 1/2 Sqrt{8Pi/15}
    DATA D1/-0.5284436396808014_WP/   ! -2/3*Sqrt{Pi/5}
    DATA D2/1.2944172750371328_WP/    ! Sqrt{8pi/15}
    DATA D3/1.4472025091165353_WP/    ! Sqrt(2Pi/3)

    INTEGER IND
    SAVE IND
    DATA IND/0/

    IF(IND.EQ.0) THEN
        CALL READ_3JS()
        IND = 1
    END IF

    A = 0.
    D1AX = 0.
    D2AX = 0.
    D1AY = 0.
    D2Ay = 0.

    K = 0
    DO L = 0, LMAX
        DO M = -L, L
            K = K+1
            A(L, M) =   AI(K)
            D1AX(L, M) = D1AIX(K)
            D2AX(L, M) = D2AIX(K)
            D1AY(L, M) = D1AIY(K)
            D2AY(L, M) = D2AIY(K)
        END DO
    END DO

    !	GO TO 1020

    ! ***** Compute D.(fDV2) = Df.DV2 + f(D.DV2)  **************!

    !  AAAA---  First for derivative for x

    ! 1. Compute DV2 = N1*DM0 - alpha*DM1 - 3/2 N*DM2

    !     E(0, 0) = PN*4*Pi/3*(-3/2) *A(0,0)
    !     E(1, :) = 4Pi/3*alpha *A(1,:)
    !     E(2, :) = C1* PN* A(2,:)

    E = 0
    E(0, 0) = PN*4*Pi/3* D1AX(0,0)*(-3/2.) + A0*PN1 * D1AX(0,0)

    !     E(1,-1) = alpha*4*pi/3* Im(D1AX, 1, 1)
    !     E(1, 0) = alpha*4*pi/3* D1AX(1, 0)
    !     E(1, 1) = alpha*4*pi/3* Re(D1AX, 1, 1)

    E(1,-1) = alpha*4*pi/3* D1AX(1,-1)
    E(1, 0) = alpha*4*pi/3* D1AX(1, 0)
    E(1, 1) = alpha*4*pi/3* D1AX(1, 1)

    E(2,-2) = PN*C1* D1AX(2,-2)
    E(2,-1) = PN*C1* D1AX(2,-1)
    E(2, 0) = PN*C1* D1AX(2, 0)
    E(2, 1) = PN*C1* D1AX(2, 1)
    E(2, 2) = PN*C1* D1AX(2, 2)

    ! 2: Compute D = Df.DV2

    CALL FTG(2, D1AX, E, D)

    !  BBBB---   Then for derivative for y

    ! 1. Compute DV2 = N1*DM0 - alpha*DM1 - 3/2 N*DM2

    E = 0
    E(0, 0) = PN*4*Pi/3* D1AY(0,0)*(-3/2.) + A0*PN1 * D1AY(0,0)

    E(1,-1) = alpha*4*pi/3* D1AY(1,-1)
    E(1, 0) = alpha*4*pi/3* D1AY(1, 0)
    E(1, 1) = alpha*4*pi/3* D1AY(1, 1)

    E(2,-2) = PN*C1* D1AY(2,-2)
    E(2,-1) = PN*C1* D1AY(2,-1)
    E(2, 0) = PN*C1* D1AY(2, 0)
    E(2, 1) = PN*C1* D1AY(2, 1)
    E(2, 2) = PN*C1* D1AY(2, 2)

    ! 2: Compute D = Df.DV2

    CALL FTG(2, D1AY, E, C)

    !  Now DF.DV2 : D = D + C.

    D = D + C

    !   CCCC---  compute f(D.DV2)

    !  Compute DDV2 = N1*DDM0 - alpha*DDM1 - 3/2 N*DDM2

    !     E(0, 0) = PN*4*Pi/3*(-3/2) *A(0,0)*(-3/2.) + A0*PN1 * D2A(0,0)
    !     E(2,:) = C1* PN* A(2,:)

    E = 0
    E(0, 0) = PN*4*Pi/3* (D2AX(0,0)+D2AY(0,0))*(-3/2.)+A0*PN1*(D2AX(0,0)+D2AY(0,0))

    E(1,-1) = alpha*4*pi/3* (D2AX(1,-1)+D2AY(1,-1))
    E(1, 0) = alpha*4*pi/3* (D2AX(1, 0)+D2AY(1, 0))
    E(1, 1) = alpha*4*pi/3* (D2AX(1, 1)+D2AY(1, 1))

    E(2,-2) = PN*C1* (D2AX(2,-2)+D2AY(2,-2))
    E(2,-1) = PN*C1* (D2AX(2,-1)+D2AY(2,-1))
    E(2, 0) = PN*C1* (D2AX(2, 0)+D2AY(2, 0))
    E(2, 1) = PN*C1* (D2AX(2, 1)+D2AY(2, 1))
    E(2, 2) = PN*C1* (D2AX(2, 2)+D2AY(2, 2))

    !  Compute (D.DV2)f

    CALL FTG(2, A, E, B)

    ! ************* Then    D.(fDV2) = D + B     **************  !

    DO L = 0, LMAX
        DO M = -L, L
            C(L, M) = PMGF*(D(L, M) + B(L, M))
            !	   C(L, M) = D(L,M)
        END DO
    END DO


1020 CONTINUE

    ! ***** Compute R.(fRV2) = Rf.RV2 + f(R.RV2)  **************!

    ! 1. Compute V2 = N1*M0 - alpha*M1 - 3/2 N*M2
    ! Note : R(Y00) = 0

    !     E(0, 0) = PN*4*Pi/3*(-3/2) *A(0,0)
    !     E(2,:) = C1* PN* A(2,:)

    E = 0
    !     E(0, 0) = PN*4*Pi/3* A(0,0)*(-3/2.)

    E(1,-1) = alpha*4*pi/3* A(1,-1)
    E(1, 0) = alpha*4*pi/3* A(1, 0)
    E(1, 1) = alpha*4*pi/3* A(1, 1)

    E(2,-2) = PN*C1* A(2,-2)
    E(2,-1) = PN*C1* A(2,-1)
    E(2, 0) = PN*C1* A(2, 0)
    E(2, 1) = PN*C1* A(2, 1)
    E(2, 2) = PN*C1* A(2, 2)

    ! 1: Compute D = Rf.RV2

    CALL RFTRG(2, A, E, D)

    ! 2: Compute (R.RV2)f

    E(0,0) = 0
    E(1,-1:1) = -2* E(1,-1:1)
    E(2,-2:2) = -6* E(2,-2:2)
    CALL FTG(2, A, E, B)

    !	write(51,*) "(R.(Rv))f= ", b

    ! ************* Then    R.(fRV2) = D + B     **************  !

    DO L = 0, LMAX
        DO M = -L, L
            B(L, M) = D(L, M) + B(L, M)
        END DO
    END DO

    !	write(51,*) "R.(f RV)= ",b


    ! *****  Now Compute  -R.(m\cross mdot f)  **************!

    D = 0

    DO L = 0, LMAX
        DO M = -L, L
            K = M - 2
            IF(K>=0) THEN
                DO P = -2, 2, 2
                    IF(L+P>=K .AND. L+P<=LMAX) THEN
                        D(L+p, K) = D(L+p, K) - Im(A, L, M)*GAMA(L,M,p/2)
                        IF(K>0) D(L+p,-K) = D(L+p,-K) + Re(A, L, M)*GAMA(L,M,p/2)
                    END IF
                END DO
            END IF
            K = M + 2
            IF(K>=0) THEN
                DO P = -2, 2, 2
                    IF(L+P>=K .AND. L+P<=LMAX) THEN
                        D(L+p, K) = D(L+p, K) + Im(A, L, M)*GAMA(L,-M,p/2)
                        IF(K>0) D(L+p,-K) = D(L+p,-K) - Re(A, L, M)*GAMA(L,-M,p/2)
                    END IF
                END DO
            END IF
        END DO
    END DO

    PE1 = DV(1,1)
    PE2 = 0.5D0*(DV(1,2) + DV(2,1))
    PE3 = 0.5D0*(DV(1,2) - DV(2,1))

    DO L = 0, LMAX
        DO M = 0, L
            E(L, M) = PA* (PE2*D(L, M) - PE1*D(L,-M)) - PE3* M * A(L,-M)
            IF(M>0) E(L,-M) = PA* (PE2*D(L,-M) + PE1*D(L, M)) + PE3* M * A(L, M)
        END DO
    END DO


    !	write(38,*) pe2, D(2,1), maxval(abs(d))

    Drr = PDE/(A(0,0)*A(0,0)*A0)
    !     Drr = PDE

    DO L = 0, LMAX
        RC = -L*(L+1)
        DO M = -L, L

            E(L, M) = PDs*(D2AX(L,M)+D2AY(L,M)+C(L,M)) + Drr*(RC*A(L,M)+B(L, M)) + E(L, M)

        END DO
    END DO

    ! add in the convection term m.grad(f)

    ! 1. Compute m_1 = \sin\theta\cos\phi = - Sqrt(2Pi/3) [Y_1^1-Y_1^{-1}]

    C = 0
    C(1, 1) = -CAO2*D3
    C(1,-1) = -CAT2*D3

    ! 2: Compute m1(dxf)

    CALL FTG(1, D1AX, C, B)

    ! Compute m_2 = \sin\theta\sin\phi = i Sqrt(2Pi/3) [Y_1^1+Y_1^{-1}]

    C = 0
    C(1, 1) = CAT2*D3
    C(1,-1) = CAO2*D3

    ! 2: Compute m2(dyf)

    CALL FTG(1, D1AY, C, D)

    ! subtract the convection term from right hand of the equation

    DO L = 0, LMAX
        DO M = -L, L
            E(L, M) = E(L, M) - PU0 * (B(L, M) + D(L, M))
        END DO
    END DO

    FV = 0

    K = 0
    DO L = 0, LMAX
        DO M = -L, L
            K = K+1
            FV(K) = E(L, M)
        END DO
    END DO


    !	write(51,*) "maxval(abs(fv))",  maxval(abs(fv))

    !	write(51,*) "max d1a ", maxval(abs(D1AI))
    !	write(51,*) "max d2a ", maxval(abs(D2AI))
    !	write(51,*) "N= ",PN
    !	write(51,*) "N1= ", PN1
    !	write(51,*) "Pe= ", Pe
    !	write(51,*) "AI= ", AI

    END SUBROUTINE DFDT


    !   *****************************************************   !
    !
    !       F*G
    !
    !   *****************************************************   !


    SUBROUTINE FTG(JM, C, B, D)
    IMPLICIT NONE
    REAL(WP), DIMENSION(0:LMAX, -LMAX:LMAX) :: C, B, D
    INTEGER :: JM, L, M, J, P, K, S, K1, K2
    REAL(WP) :: VR, VI

    D = 0

    DO L = 0, LMAX
        DO M = -L, L
            K1 = L*(L+1)+M
            DO J = 0, JM
                DO P = -J, J
                    K2 = J*(J+1)+P
                    K = M+P
                    IF(K>=0) THEN
                        VR = Re(C,L,M)*Re(B,J,P)-Im(C,L,M)*Im(B,J,P)
                        VI = Re(C,L,M)*Im(B,J,P)+Im(C,L,M)*Re(B,J,P)
                        DO S = MAX(ABS(L-J),K), MIN(L+J, LMAX)
                            IF(MOD(K,2)==0) THEN
                                D(S, K) = D(S, K) + VR * SIGMA(K1,K2,S)
                                IF(K/=0) D(S,-K) = D(S,-K) + VI * SIGMA(K1,K2,S)
                            ELSE
                                D(S, K) = D(S, K) - VR * SIGMA(K1,K2,S)
                                D(S,-K) = D(S,-K) - VI * SIGMA(K1,K2,S)
                            END IF
                        END DO  ! S
                    END IF  ! (K>=0)
                END DO   ! P
            END DO   ! J
        END DO   ! M
    END DO   ! L


    END SUBROUTINE FTG


    !   *****************************************************   !
    !
    !       RF.RG
    !
    !   *****************************************************   !


    SUBROUTINE RFTRG(JM, A, D, C)
    IMPLICIT NONE
    REAL(WP), DIMENSION(0:LMAX, -LMAX:LMAX) :: A, D, C
    INTEGER :: JM, L, M, J, P, K, S, K1, K2
    REAL(WP) :: VR, VI

    C = 0

    DO L = 0, LMAX
        DO M = -L, L
            K1 = L*(L+1)+M
            DO J = 0, JM
                DO P = -J, J
                    K2 = J*(J+1)+P
                    K = M+P
                    IF(K>=0) THEN
                        VR = Re(A,L,M)*Re(D,J,P)-Im(A,L,M)*Im(D,J,P)
                        VI = Re(A,L,M)*Im(D,J,P)+Im(A,L,M)*Re(D,J,P)
                        DO S = MAX(ABS(L-J),K), MIN(L+J, LMAX)
                            IF(MOD(K,2)==0) THEN
                                C(S, K) = C(S, K) + VR * BETA(K1,K2,S)
                                IF(K>0) C(S,-K) = C(S,-K) + VI * BETA(K1,K2,S)
                            ELSE
                                C(S, K) = C(S, K) - VR * BETA(K1,K2,S)
                                C(S,-K) = C(S,-K) - VI * BETA(K1,K2,S)
                            END IF
                        END DO  ! S
                    END IF  ! (K>=0)
                END DO   ! P
            END DO   ! J
        END DO   ! M
    END DO   ! L

    END SUBROUTINE RFTRG


    FUNCTION Re(A, L, M)
    REAL(WP) :: Re
    REAL(WP), DIMENSION(0:LMAX,-LMAX:LMAX) :: A
    INTEGER :: L, M

    IF(M>=0) THEN
        RE = A(L,M)
    ELSE
        IF(MOD(M,2)==0) THEN
            RE = A(L,-M)
        ELSE
            RE = -A(L,-M)
        END IF
    END IF

    END FUNCTION Re



    FUNCTION Im(A, L, M)
    REAL(WP) :: IM
    REAL(WP), DIMENSION(0:LMAX,-LMAX:LMAX) :: A
    INTEGER :: L, M

    IF(M==0) THEN
        IM = 0
    ELSE IF(M>0) THEN
        IM = A(L,-M)
    ELSE
        IF(MOD(M,2)==0) THEN
            IM = -A(L, M)
        ELSE
            IM =  A(L, M)
        END IF
    END IF

    END FUNCTION Im



    END MODULE ACP
